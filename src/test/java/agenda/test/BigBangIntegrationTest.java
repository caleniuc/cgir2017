package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class BigBangIntegrationTest {
    private Contact con;
    private RepositoryContact repContact;
    private RepositoryActivity repActivity;

    public BigBangIntegrationTest(){
        repContact = new RepositoryContactMock();
        repActivity = new RepositoryActivityMock();
    }

    @Test
    public void testAddContactValid() {
        try {
            con = new Contact("Andrei", "Strada Primaverii", "0758574839", "andrei@email.com");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        //int n = rep.count();
        repContact.addContact(con);
        for (Contact c : repContact.getContacts())
            if (c.equals(con)) {
                assertTrue(true);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }

    @Test
    public void testCaseValid()
    {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try{
            for (Activity a : repActivity.getActivities())
                repActivity.removeActivity(a);

            Activity act = new Activity("user",
                    df.parse("04/23/2018 08:30"),
                    df.parse("04/23/2018 09:30"),
                    null,
                    "desc1", "parc1");

            assertTrue(repActivity.addActivity(act));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testCasePrintActivities() {
        Calendar c = Calendar.getInstance();
        c.set(2013, Calendar.MARCH,  20,12,0, 0);
        List<Activity> result = repActivity.activitiesOnDate("user", c.getTime());

        assertTrue(result.size() == 1);
    }

    @Test
    public void testCaseIntegration() {
        boolean part1 = false, part2 = false;

        try {
            repContact.addContact((Contact) new Object());
        } catch (Exception e) {
            part1 = true;
        }

        Activity act = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int count = repActivity.count();
        try {
            act = new Activity("Andrei", df.parse("04/22/2018 12:00"),
                    df.parse("04/22/2018 13:00"), null, "Lunch break", "location1");
            repActivity.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (repActivity.count() == count + 1 && repActivity.getActivities().get(count).equals(act))
            part2 = true;

        Calendar c = Calendar.getInstance();
        c.set(2018, Calendar.APRIL, 22, 12, 0, 0);

        List<Activity> result = repActivity.activitiesOnDate("Andrei", c.getTime());
        assertTrue(result.size() == 1 && result.get(0).equals(act) && part1
                && part2);
    }
}
