package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TopDownTest {
    private Contact con;
    private RepositoryContact repContact;
    private RepositoryActivity repActivity;

    public TopDownTest(){
        repContact = new RepositoryContactMock();
        repActivity = new RepositoryActivityMock();
    }

    @Test
    public void testAddContactValid() {
        try {
            con = new Contact("Andrei", "Strada Primaverii", "0758574839", "andrei@email.com");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        //int n = rep.count();
        repContact.addContact(con);
        for (Contact c : repContact.getContacts())
            if (c.equals(con)) {
                assertTrue(true);
                break;
            }
        //assertTrue(n+1 == rep.count());
    }

    @Test
    public void testCaseIntegrationB() {
        boolean part1 = false, part2 = false;

        try {
            repContact.addContact((Contact) new Object());
        } catch (Exception e) {
            part1 = true;
        }

        Activity act = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int count = repActivity.count();
        try {
            act = new Activity("Andrei", df.parse("04/22/2018 12:00"),
                    df.parse("04/22/2018 13:00"), null, "Lunch break", "location1");
            repActivity.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (repActivity.count() == count + 1 && repActivity.getActivities().get(count).equals(act))
            part2 = true;

        assertTrue(part1
                && part2);
    }

    @Test
    public void testCaseIntegrationC() {
        boolean part1 = false, part2 = false;

        try {
            repContact.addContact((Contact) new Object());
        } catch (Exception e) {
            part1 = true;
        }

        Activity act = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int count = repActivity.count();
        try {
            act = new Activity("Andrei", df.parse("04/22/2018 12:00"),
                    df.parse("04/22/2018 13:00"), null, "Lunch break", "location2");
            repActivity.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (repActivity.count() == count + 1 && repActivity.getActivities().get(count).equals(act))
            part2 = true;

        Calendar c = Calendar.getInstance();
        c.set(2018, Calendar.APRIL, 22, 12, 0, 0);

        List<Activity> result = repActivity.activitiesOnDate("Andrei", c.getTime());
        assertTrue(result.size() == 1 && result.get(0).equals(act) && part1
                && part2);
    }


}
